#Nicolas Dufour

FROM node as build

WORKDIR /test

COPY . .

RUN npm install -g gatsby-cli
RUN npm install
RUN gatsby build


FROM nginx

COPY --from=build test/public /usr/share/nginx/html